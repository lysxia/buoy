# Numeric buoy

Floating-point computations with explicit error terms.

# Related

## Interval arithmetic

- [aern2-mp](https://hackage.haskell.org/package/aern2-mp-0.1.4)

- (A non-solution, doesn't track precision) [interval](https://hackage.haskell.org/package/data-interval)

## Infinite-precision numbers

- [numbers](https://hackage.haskell.org/package/numbers-3000.2.0.2), `CReal`

- [ireal](https://hackage.haskell.org/package/ireal)

- [aern2-real](http://hackage.haskell.org/package/aern2-real),
  "exact real numbers as Cauchy sequences of MPFR approximations."

- [exact-real](https://hackage.haskell.org/package/exact-real),
  "A type to represent exact real numbers using fast binary Cauchy sequences."

- [cf](https://hackage.haskell.org/package/cf), "continued fractions."

## Arbitrary-precision numbers

- [rounded](https://hackage.haskell.org/package/rounded-0.1.0.1)

## Fixed-point numbers

- base, [`Data.Fixed`](https://hackage.haskell.org/package/base-4.12.0.0/docs/Data-Fixed.html)
- [fixed](https://hackage.haskell.org/package/fixed)

## General decimal arithmetic

http://speleotrove.com/decimal/

- [decimal-arithmetic](https://hackage.haskell.org/package/decimal-arithmetic)
  (rounds)
- [deka](https://hackage.haskell.org/package/deka)
  (only exact arithmetic, errors if result cannot be represented, no division)

## Static analysis

- *Faithfully rounded floating-point computations*, M. Lange and S. M. Rump, 2017 ([PDF][frfpc]).

[frfpc]: http://www.ti3.tu-harburg.de/paper/rump/LaRu2017b.pdf

## TODO

https://hackage.haskell.org/package/compensated (and the references therein)

https://hackage.haskell.org/package/factory

https://www.researchgate.net/publication/304671552_Interval_arithmetic_with_fixed_rounding_mode
