module Numeric.Buoy
  ( Buoy()

    -- * 'Buoy' constructors
  , buoy, ball, exact

    -- * 'Buoy' accessors
  , lower, upper, center
  ) where

import Numeric.Buoy.Unsafe (Buoy, buoy, ball, exact)
import qualified Numeric.Buoy.Unsafe as Unsafe

-- | Lower bound of a 'Buoy'.
lower :: Buoy -> Double
lower = Unsafe.lower

-- | Upper bound of a 'Buoy'.
upper :: Buoy -> Double
upper = Unsafe.upper

-- | Center value of a 'Buoy'.
--
-- Note: this is not the average of the lower and upper bound.
center :: Buoy -> Double
center = Unsafe.center
