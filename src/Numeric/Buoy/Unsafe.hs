{-# LANGUAGE StrictData #-}

module Numeric.Buoy.Unsafe where

import Data.Function (on)

import qualified Numeric.Buoy.Interval as Interval
import Numeric.Buoy.Interval (Interval(Interval))

-- | A 'Buoy' is a floating-point value ('Double') that keeps track of its own
-- accuracy.
--
-- Internally, it is a value with a lower and upper bound.
-- Operations (in 'Num', 'Fractional') make sure to not forget any potential
-- value within the bounded interval, overapproximating where necessary.
--
-- In principle, the semantics of a 'Buoy' are "any value in the interval",
-- which makes the center value meaningless.
--
-- But in exchange for giving up some formal guarantees,
-- the center value allows comparisons (which are ill-defined on intervals),
-- and to extract a final 'Double' result.
-- It also records some information about how nonlinear transformations skew
-- the interval (multiplication and division do that).
data Buoy = Buoy
  { lower :: {-# UNPACK #-} Double
  , center :: {-# UNPACK #-} Double
  , upper :: {-# UNPACK #-} Double
  } deriving Show

instance Eq Buoy where
  (==) = (==) `on` center

instance Ord Buoy where
  compare = compare `on` center

-- | 'Buoy' constructor.
--
-- @'buoy' x y z@ is @y@ in the interval @[x, z]@.
-- Requires @x <= y <= z@. Otherwise, it is an error.
buoy :: Double -> Double -> Double -> Buoy
buoy x y z
  | x <= y && y <= z = Buoy x y z
  | otherwise = error ("buoy: ill-formed interval " ++ show (x, y, z))

-- | Alternative 'Buoy' constructor.
--
-- @'ball' x e@ is @x@ in the interval @[x-e, x+e]@.
-- Requires @0 <= e@. Otherwise, it is an error.
ball :: Double -> Double -> Buoy
ball x e
  | 0 <= e = Buoy (x - e) x (x + e)
  | otherwise = error ("ball: ill-formed interval " ++ show (x, e))

-- | Singleton 'Buoy' constructor.
--
-- @'exact' x@ is @x@ in the interval @[x, x]@.
exact :: Double -> Buoy
exact x = Buoy x x x

type IDouble = Interval Double

fromBuoy :: Buoy -> IDouble
fromBuoy i = Interval (lower i) (upper i)

unsafeToBuoy :: Double -> IDouble -> Buoy
unsafeToBuoy x i = Buoy (Interval.lower i) x (Interval.upper i)

unop :: (Double -> Double) -> (IDouble -> IDouble) -> Buoy -> Buoy
unop fx fi i = unsafeToBuoy (fx (center i)) (fi (fromBuoy i))

binop ::
  (Double -> Double -> Double) ->
  (IDouble -> IDouble -> IDouble) ->
  Buoy -> Buoy -> Buoy
binop fx fi i j = unsafeToBuoy (fx (center i) (center j)) (fi (fromBuoy i) (fromBuoy j))

instance Num Buoy where
  (+) = binop (+) (+)
  (*) = binop (*) (*)
  negate = unop negate negate
  abs = unop abs abs
  signum = unop signum signum
  fromInteger = exact . fromInteger

instance Fractional Buoy where
  (/) = binop (/) (/)
  fromRational x = unsafeToBuoy (fromRational x) (fromRational x)
