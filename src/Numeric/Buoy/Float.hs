{- | Low-level floating-point manipulations.
-}
module Numeric.Buoy.Float where

import GHC.Float (isDoubleFinite)
import Numeric.IEEE (IEEE)

-- * Almost exact floating-point operations.
--
-- $almost

-- $almost
-- These operations store additional low-order bits in an auxiliary 'Double'.
--
-- === \"Almost exact\"
--
-- - 'add' is exact.
-- - 'times' loses some information if the result is too close to 0.
-- - 'divide' loses some information if the dividend is too close to 0.

-- | @'add' a b k@ computes @k x y@ such that
--
-- > x + y = a + b
-- > x = fl(a + b)
--
-- Which is to say that @x@ is the floating point image of @(a + b)@ and
-- @y@ stores the residual error term.
--
-- From the /compensated/ library.
add :: Num a => a -> a -> (a -> a -> r) -> r
add a b k = k x y where
  x = a + b
  z = x - a
  y = (a - (x - z)) + (b - z)
{-# INLINE add #-}

-- | @'times' a b k@ computes @k x y@ such that
--
-- > x + y' = a * b
-- > x = fl(a * b)
-- >
-- > where y' = if 2^(-900) <= x then y else (2^(-1000) * (y + epsilon))
-- > and epsilon is a negligible term compared to y.
--
-- The adjustment on @y@ is made to not lose higher bits
-- when @x@ gets too close to 0.
--
-- The exact value of @y@ doesn't actually matter here,
-- we only use the sign of @y@ to keep track of which way @x@ was rounded.
--
-- Modified from the /compensated/ library.
times :: MagicFloat a => a -> a -> (a -> a -> r) -> r
times a b k =
  let x = a * b in
  if magic2 <= x then
    split a (\a1 a2 ->
    split b (\b1 b2 ->
    k x (a2*b2 - (((x - a1*b1) - a2*b1) - a1*b2))))
  else
    let a' = magic3 * a in
    let b' = magic3 * b in
    split a' (\a1 a2 ->
    split b' (\b1 b2 ->
    let x' = x * magic3 * magic3 in
    k x (a2*b2 - (((x' - a1*b1) - a2*b1) - a1*b2))))
{-# INLINE times #-}

-- | Error-free split of a floating point number into two parts.
-- 
-- From the /compensated/ library.
split :: MagicFloat a => a -> (a -> a -> r) -> r
split a k = k x y where
  c = magic * a
  x = c - (c - a)
  y = a - x
{-# INLINE split #-}

-- | Returns a triple @:(q, w, r')@ such that:
--
-- > q = fl(a / b)
-- > w = a - (q * b) + r
-- >
-- > where r = r' except if r is too small, then it's scaled up to fit,
-- > with some loss (see also 'times').
-- >
-- > if w > 0 || (w == 0 && r' < 0) then q < (a / b)
-- > if w < 0 || (w == 0 && r' > 0) then q > (a / b)
-- > if w == 0 && r' == 0 then q = a / b
divide :: MagicFloat a => a -> a -> (a -> a -> a -> r) -> r
divide a b k =
  let x = a / b in
  times x b (\a' r -> k x (a - a') r)

-- | Magic constants for floating-point types.
class (Show a, IEEE a) => MagicFloat a where
  magic, magic2, magic3 :: a

instance MagicFloat Double where
  magic = 134217729          -- @2 ^ 27@
  magic2 = 2^^(-900 :: Int)
  magic3 = 2^^(500 :: Int)

-- | Not NaN and not infinity.
isFinite :: Double -> Bool
isFinite = (/= 0) . isDoubleFinite
