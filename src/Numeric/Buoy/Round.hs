{-# LANGUAGE BangPatterns #-}

module Numeric.Buoy.Round where

import Data.Ratio (Ratio)
import Numeric.IEEE (succIEEE, predIEEE)
import qualified Numeric.Buoy.Float as Float

data Rounding = RoundUp | RoundDown

turnover :: Rounding -> Rounding
turnover RoundUp = RoundDown
turnover RoundDown = RoundUp

class (Num a, Ord a) => Round a where
  (+^) :: a -> a -> Rounding -> a
  (*^) :: a -> a -> Rounding -> a
  (/^) :: a -> a -> Rounding -> a
  infinity :: a
  roundRational :: Rational -> Rounding -> a

instance Integral a => Round (Ratio a) where
  (x +^ y) _ = x + y
  (x *^ y) _ = x * y
  (x /^ y) _ = x / y
  infinity = error "(/^): divided by zero"
  roundRational x _ = fromRational x

-- Note [NaN handling]:
--
-- The @round_@ functions below look at a residual @r@ to decide whether
-- to bump the result up or down ('predIEEE', 'succIEEE').
-- If that residual is @NaN@, we bump the result conservatively.
-- This is why they are always in the 'otherwise' branch.
-- This relies on comparisons evaluating to 'False' on @NaN@.

instance Round Double where

  {-# INLINE (+^) #-}
  (x +^ y) !g = Float.add x y round_ where
    round_ s r = case g of
      RoundDown
        | r >= 0 -> s
        | otherwise -> predIEEE s
      RoundUp
        | r <= 0 -> s
        | otherwise -> succIEEE s

  {-# INLINE (*^) #-}
  (x *^ y) !g = Float.times x y round_ where
    round_ s r = case g of
      RoundDown
        | r >= 0 -> s
        | otherwise -> predIEEE s
      RoundUp
        | r <= 0 -> s
        | otherwise -> succIEEE s

  {-# INLINE (/^) #-}
  (x /^ y) !g = Float.divide x y round_ where
    round_ q w r = case g of
      RoundDown
        | w > 0 || (w == 0 && r <= 0) -> q
        | otherwise -> predIEEE q
      RoundUp
        | w < 0 || (w == 0 && r >= 0 && not (isInfinite r)) -> q
        | otherwise -> succIEEE q

  infinity = 0/1

  roundRational x _ = fromRational x
