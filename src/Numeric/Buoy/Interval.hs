{-# LANGUAGE StrictData #-}

module Numeric.Buoy.Interval where

import Numeric.Buoy.Round

data Interval a = Interval
  { lower, upper :: a
  } deriving Show

interval :: Ord a => a -> a -> Interval a
interval x y
  | x <= y = Interval x y
  | otherwise = error "interval: empty interval"

instance Round a => Num (Interval a) where
  x + y = Interval
    { lower = (lower x +^ lower y) RoundDown
    , upper = (upper x +^ upper y) RoundUp
    }

  x * y =
    -- x interval is entirely nonnegative
    if 0 <= lower x then
      let lowerx = if 0 <= lower y then lower x else upper x
          upperx = if upper y <= 0 then lower x else upper x
      in Interval
        { lower = (lowerx *^ lower y) RoundDown
        , upper = (upperx *^ upper y) RoundUp
        }
    -- x interval is entirely nonpositive
    else if upper x <= 0 then
      let lowerx = if upper y <= 0 then upper x else lower x
          upperx = if 0 <= lower y then upper x else lower x
      in Interval
        { lower = (lowerx *^ upper y) RoundDown
        , upper = (upperx *^ lower y) RoundUp
        }
    -- 0 cuts x interval in two
    else
      if 0 <= lower y then Interval
        { lower = (lower x *^ upper y) RoundDown
        , upper = (upper x *^ upper y) RoundUp
        }
      else if upper y <= 0 then Interval
        { lower = (upper x *^ lower y) RoundDown
        , upper = (lower x *^ lower y) RoundUp
        }
      else Interval
        { lower = min ((lower x *^ upper y) RoundDown) ((upper x *^ lower y) RoundDown)
        , upper = max ((lower x *^ lower y) RoundUp)   ((upper x *^ upper y) RoundUp)
        }

  negate x = Interval
    { lower = negate (upper x)
    , upper = negate (lower x)
    }

  abs x =
    if 0 <= lower x then x
    else if upper x <= 0 then Interval
      { lower = abs (upper x)
      , upper = abs (lower x)
      }
    else Interval
      { lower = 0
      , upper = max (abs (lower x)) (upper x)
      }

  -- | @0@ if the interval contains 0.
  signum x =
    if 0 < lower x then 1
    else if upper x < 0 then -1
    else 0

  fromInteger n = Interval n' n' where n' = fromInteger n

instance Round a => Fractional (Interval a) where
  x / y =
    if 0 < lower y then x /+ y
    else if upper y < 0 then negate (x /+ negate y)
    else Interval
      { lower = - infinity
      , upper = infinity
      }

  fromRational x = Interval (roundRational x RoundDown) (roundRational x RoundUp)

-- | Division assuming @y@ is positive.
(/+) :: Round a => Interval a -> Interval a -> Interval a
x /+ y = Interval
  { lower = (lower x /^ lowery) RoundDown
  , upper = (upper x /^ uppery) RoundUp
  } where
  lowery = if 0 <= lower x then upper y else lower y
  uppery = if upper x <= 0 then upper y else lower y
