module Numeric.Buoy.Pretty where

import Data.Bifunctor (bimap)
import Data.Char (intToDigit)
import Numeric (floatToDigits)

import qualified Numeric.Buoy.Float as Float (isFinite)
import Numeric.Buoy.Unsafe

pretty :: Buoy -> String
pretty i
  | not (Float.isFinite (lower i) && Float.isFinite (upper i))
  = prettyInterval i
  | 0 <= lower i = prettyDiff (diff i)
  | upper i <= 0 = "-" ++ prettyDiff (swapDiff (diff (negate i)))
  | otherwise = prettyInterval i

prettyInterval :: Buoy -> String
prettyInterval i = "[" ++ show (lower i) ++ "~" ++ show (upper i) ++ "]"

swapDiff :: Diff -> Diff
swapDiff d = d { suffix = fmap swap (suffix d) }

swap :: (a, b) -> (b, a)
swap (x, y) = (y, x)

data Diff = Diff
  { prefix :: [Int]
  , suffix :: [(Int, Int)] 
  , expo :: Int
  }

-- This logic looks a bit horrible
diff :: Buoy -> Diff
diff i = diff_ [] low upp
  where
    base = 10
    (low_, lowe) = floatToDigits base (lower i)
    (upp_, uppe) = floatToDigits base (upper i)

    (low, upp, e)
      | lowe <= uppe = (replicate (uppe - lowe) 0 ++ low_, upp_, uppe)
      | otherwise = (low_, replicate (lowe - uppe) 0 ++ upp_, lowe)

    diff_ acc (x : xs) (y : ys)
      | x == y = diff_ (x : acc) xs ys
    diff_ acc xs (9 : 9 : _) = incr acc (zips xs [0, 0])
    diff_ acc xs (y : 9 : _ : _) = Diff
      { prefix = reverse acc
      , suffix = zips xs [y + 1, 0]
      , expo = e
      }
    diff_ acc xs ys = Diff
      { prefix = reverse acc
      , suffix = take 2 (zips xs ys)
      , expo = e
      }

    zips xs [] = []
    zips [] (y : ys) = (0, y) : zips [] ys
    zips (x : xs) (y : ys) = (x, y) : zips xs ys

    incr (9 : acc) suf = incr acc ((9, 0) : suf)
    incr [] suf = Diff
      { prefix = []
      , suffix = (0, 1) : suf
      , expo = e + 1
      }
    incr (x : acc) suf = Diff
      { prefix = reverse acc
      , suffix = (x, x + 1) : suf
      , expo = e
      }

prettyDiff :: Diff -> String
prettyDiff d@Diff{prefix = d0 : pre, expo = e0}
  | e0 <= 7,
    let pad = e0 - (1 + length pre + length (suffix d)),
    0 <= pad
  = let padDigits = replicate pad '0' in
    headDigit : prefixDigits ++ diffDigits ++ padDigits

  | otherwise
  = headDigit : '.' : prefixDigits ++ diffDigits ++ showExp (e0 - 1)
  where
    headDigit = intToDigit d0
    prefixDigits = map intToDigit pre
    diffDigits
      | [] <- suffix d = ""
      | otherwise = '[' : getSuffix fst ++ '~' : getSuffix snd ++ "]"
    getSuffix f = map (intToDigit . f) (suffix d)

-- Take two digits, in case the difference is caused by the second digit being a 9.
-- [19~20] looks more accurate than [1~2]
prettyDiff d@Diff{prefix = [], expo = e0} =
  '[' : x0 : '.' : x1 : '~' : y0 : '.' : y1 : ']' : showExp (e0 - 1)
  where
    (x0, y0) : (x1, y1) : _ =
      ((++ repeat ('0', '0')) . map (bimap intToDigit intToDigit) . suffix) d

showExp :: Int -> String
showExp 0 = ""
showExp e = "e" ++ show e

hd :: a -> [a] -> a
hd x [] = x
hd _ (x : _) = x
