{-# LANGUAGE BlockArguments #-}
{-# OPTIONS_GHC -Wno-orphans #-}

import Control.Applicative (liftA3)

import Data.Validity
import Data.GenValidity.Utils (genDouble)
import Data.GenValidity
import Test.QuickCheck

import Numeric.Buoy
import Numeric.Buoy.Interval (Interval(Interval))
import qualified Numeric.Buoy.Interval as Interval
import qualified Numeric.Buoy.Float as Float

instance Validity Buoy where
  validate x =
    check (Float.isFinite (lower x)) "lower bound is finite" <>
    check (Float.isFinite (upper x)) "upper bound is finite" <>
    check (lower x <= upper x) "is nonempty interval"

genDouble' :: Gen Double
genDouble' = genDouble `suchThat` Float.isFinite

buoy' :: Double -> Double -> Double -> Buoy
buoy' x y z = buoy (min x y) y (max y z)

instance GenValid Buoy where
  genValid = liftA3 buoy' genDouble' genDouble' genDouble' where

  shrinkValid x = filter (not . identical x)
    [buoy' low v high | (low, v, high) <- shrinkValid (lower x, center x, upper x)]

identical :: Buoy -> Buoy -> Bool
identical x y = triple x == triple y where
  triple i = (lower i, center i, upper i)

genGoodDoubles :: Gen (Double, Double)
genGoodDoubles = genValid `suchThat` \(a, b) ->
  Float.isFinite (a + b)

prop_add :: Property
prop_add = forAllShrink genGoodDoubles shrinkValid \(a, b) ->
  Float.add (a :: Double) b \x y ->
    (x, x + y) === (a + b, x)  -- y is negligible compared to x

type IRatio = Interval Rational

le :: Rational -> Rational -> Property
le x y = counterexample ("Failed inequality: " ++ show x ++ " <= " ++ show y)
  (x <= y)

subset :: IRatio -> IRatio -> Property
subset x y = (Interval.lower y `le` Interval.lower x) .&&. (Interval.upper x `le` Interval.upper y)

toIRatio :: Buoy -> IRatio
toIRatio x = Interval (toRational (lower x)) (toRational (upper x))

genGoodBuoys :: (Buoy -> Buoy -> Buoy) -> Gen (Buoy, Buoy)
genGoodBuoys f = genValid `suchThat` isGoodBuoys f

isGoodBuoy :: Buoy -> Bool
isGoodBuoy x = Float.isFinite (upper x) && Float.isFinite (lower x)

isGoodBuoys :: (Buoy -> Buoy -> Buoy) -> (Buoy, Buoy) -> Bool
isGoodBuoys f (a, b) = isGoodBuoy a && isGoodBuoy b && isGoodBuoy (f a b)

mkPropOp :: (Buoy -> Buoy -> Buoy) -> (IRatio -> IRatio -> IRatio) -> Property
mkPropOp op op' = forAllShrink (genGoodBuoys (*)) shrinkValid \(x, y) ->
  let x' = toIRatio x
      y' = toIRatio y
      xy = x `op` y
      xy' = x' `op'` y'
      xy0 = toIRatio xy in
  counterexample (show xy) $
  counterexample (show (x', y')) $
  counterexample (show (xy', xy0)) $
  xy' `subset` xy0

prop_addBuoy :: Property
prop_addBuoy = mkPropOp (+) (+)

prop_mulBuoy :: Property
prop_mulBuoy = mkPropOp (*) (*)

shrinkSuchThat :: (GenValid a, Show a) => (a -> Bool) -> a -> [a]
shrinkSuchThat f = filter f . shrinkValid

nonZeroBuoy :: Buoy -> Bool
nonZeroBuoy x = 0 < lower x || upper x < 0

nonZeroBuoys :: (Buoy, Buoy) -> Bool
nonZeroBuoys xy@(_, y) = nonZeroBuoy y && isGoodBuoys (/) xy

prop_divBuoy :: Property
prop_divBuoy = forAllShrink
  (genValid `suchThat` nonZeroBuoys)
  (shrinkSuchThat nonZeroBuoys) \(x, y) ->
  let x' = toIRatio x
      y' = toIRatio y
      xy' = x' / y'
      xy = x / y
      xy0 = toIRatio xy in
  counterexample (show xy) $
  counterexample (show (x', y')) $
  counterexample (show (xy', xy0)) $
  xy' `subset` xy0

prop_divide :: Property
prop_divide = forAllShrink (genGoodDoubles `suchThat` \(_, y) -> y /= 0) shrink' \(x, y) ->
   Float.divide x y \q w r ->
     let q' = toRational x / toRational y in
     Float.isFinite r ==>
       if (w > 0 || (w == 0 && r <= 0)) `xor` (y < 0) then toRational q `le` q' else q' `le` toRational q
 where shrink' xy = [xy' | xy' <- shrink xy, snd xy' /= 0]
       xor = (/=)

big :: Args
big = stdArgs{maxSize=1000, maxSuccess=10000}

sanityCheck :: IO ()
sanityCheck =
  quickCheck $ \x -> not (Float.isFinite x) == (isNaN x || isInfinite x)

arithCheck :: IO ()
arithCheck = do
  quickCheckWith big prop_add
  quickCheckWith big prop_divide

buoyCheck :: IO ()
buoyCheck = do
  quickCheckWith big prop_addBuoy
  quickCheckWith big prop_mulBuoy
  quickCheckWith big prop_divBuoy

main :: IO ()
main = do
  sanityCheck
  arithCheck
  buoyCheck
